# Data visualisation
These are experiments in data visualisation. The main intention is to see how I can get these to work from RDF data, e.g. RDF export from EPrints/WRAP http://wrap.warwick.ac.uk.

There's aslo work with CoffeeScript. The CoffeeScript source files are in src/. The CakeFile used to generate them is in the main repo. The output goes to a directory called lib/
## Force clickable
This is a port of http://bl.ocks.org/mbostock/1062288 to CoffeeScript. First attempt at porting, so had to figure out how to rewrite the original JavaScript functions