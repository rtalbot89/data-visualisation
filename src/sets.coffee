rows = [
  #students/undergraduates/using
  {L1: "home", L2:"", L3: "", L4:""}
  {L1: "", L2:"students", L3:"", L4: ""}
  {L1: "", L2:"", L3:"undergraduates", L4:""}
  {L1: "", L2:"", L3:"", L4:"using the library"}
  #search/encore
  {L1: "", L2 : "search", L3:"", L4: ""}
  {L1: "", L2 : "", L3: "encore", L4: ""}
  {L1: "", L2 : "", L3:"", L4:"good ideas"}
]

ref = (obj, str) ->
  obj = obj[str]

siteMap = {name: "home", children : []}

refObj = ref(siteMap, "children")
refObj.push({name: "staff", children: []})

refObj = ref(refObj[0],"children")
refObj.push({name: "research", children: []})

refObj = ref(refObj[0], "children")
refObj.push({name: "open access", children: []})

refObj = ref(refObj[0], "children")
refObj.push({name: "medline"})

if not refObj.children?
  refObj.children = []

console.log siteMap


  
