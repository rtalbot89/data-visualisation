root = exports ? this
Network = (nodes, links) ->
  width = 960
  height = 500
  color = d3.scale.category20()
  exitLinks = []
  exitNodes = []
  tick = ->
    link.attr("x1", (d) ->
      d.source.x
    ).attr("y1", (d) ->
      d.source.y
    ).attr("x2", (d) ->
      d.target.x
    ).attr "y2", (d) ->
      d.target.y
    gnodes.attr("transform", (d) -> 'translate(' + [d.x, d.y] + ')')

  force = d3.layout.force()
  .nodes(nodes)
  .links(links)
  .charge(-400)
  .linkDistance(120)
  .size([width, height])
  .on("tick", tick)

  svg = d3.select("body")
  .append("svg")
  .attr("width", width).attr("height", height)

  gnodes = svg.selectAll("g.gnode")
  link = svg.selectAll(".link")

  buttons = d3.select("body")
  .selectAll("input").attr("type","checkbox")
  .data(nodes).enter()
  .append("label", (d) -> d.id).text((d) -> "Remove #{d.id}")
  .append("input").attr("type","checkbox")
  .on("click", (d) -> removeNode(d.id, this.checked))

  start = ->
    link = link.data(force.links(), (d) ->
      d.source.id + "-" + d.target.id
    )
    link.enter().insert("line", ".node").attr "class", "link"
    link.exit().remove()

    gnodes = gnodes.data(force.nodes(), (d) ->
      d.id
    )
    nodeEnter = gnodes.enter().append("g").classed("gnode", true)
    nodeEnter.append("circle").attr("class", (d) -> "node" + d.id )
      .attr("r", 8).attr("fill",(d,i) -> color(i))
    nodeEnter.append("text").text((d) -> d.id).attr("x", 10)
    gnodes.exit().remove()
    force.start()

  start()

  removeNode = (nid, checkstate) ->
    if checkstate is true
      for item, i in nodes
        if item.id is nid
          removedNode = nodes.splice(i,1)
          exitNodes.push(removedNode[0])
          break
      for item, i in links by -1
        if item.source.id is nid or item.target.id is nid
          removedLink = links.splice(i,1)
          exitLinks.push(removedLink[0])
      start()
    else
      for eNode,i in exitNodes
        if eNode.id is nid
          nodes.push(eNode)
          exitNodes.splice(i,1)
          break

      for elink,i in exitLinks by -1
        if elink.source.id is nid or elink.target.id is nid
          # to avoid hanging links the source can be this node
          # and have to check that the target node is visible
          # also have to check the other way round, i.e. that
          # where this is a target the source is visible
          findSource = (sourceNode for sourceNode in nodes when sourceNode.id is elink.source.id)
          findTarget = (targetNode for targetNode in nodes when targetNode.id is elink.target.id)
          if findSource.length isnt 0 and findTarget.length isnt 0
            links.push(elink)
            exitLinks.splice(i,1)
      start()

a = id: "a"
b = id: "b"
c = id: "c"
d = id: "d"
e = id: "e"
f = id: "f"

nodes = [a, b, c, d, e, f]
links = [
  {source: a, target: b}
, 
  {source: b, target: c}
, 
  {source: c, target: d}
,
  {source: d, target: e}
,
  {source: e, target: f}
,
  {source: f, target: a}
]
  
$ ->
  myNetwork = Network(nodes, links)
  