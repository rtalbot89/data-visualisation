root = null
width = 960 
height = 500
nodes = [] 

force = d3.layout.force()
  .size([width,height])
  .on("tick", () ->
    link.attr("x1", (d) -> d.source.x)
      .attr("y1", (d) -> d.source.y)
      .attr("x2", (d) -> d.target.x)
      .attr("y2", (d) -> d.target.y)
    
    node.attr("cx", (d) -> d.x)
        .attr("cy", (d) -> d.y)
    )

svg = d3.select("body").append("svg")
  .attr("width", width)
  .attr("height", height)

link = svg.selectAll(".link")
node = svg.selectAll(".node")

# looks like this is where the data goes in so could
# be quite handy with ajax
d3.json("readme.json", (json) ->  
  root = json 
  update()
)

update = () ->
  nodes = flatten(root)
  links = d3.layout.tree().links(nodes)
  
  # Restart the force layout.
  force
    .nodes(nodes)
    .links(links)
    .start()
  
  # Update the links…
  link = link.data(links, (d) -> d.target.id)
  
  # Exit any old links
  link.exit().remove()
  
  # Enter any new links.
  link.enter().insert("line",".node")
  .attr("class", "link")
  .attr("x1", (d) -> d.source.x)
  .attr("y1", (d) ->  d.source.y)
  .attr("x2", (d) -> d.target.x)
  .attr("y2", (d) -> d.target.y)
  
  # Update the nodes…
  node = node.data(nodes, (d) -> d.id).style("fill", color)
  
  # Exit any old nodes.
  node.exit().remove()
  
  # Enter any new nodes.
  node.enter().append("circle")
  .attr("class", "node")
  .attr("cx", (d) -> d.x)
  .attr("cy", (d) ->  d.y)
  .attr("r", (d) -> Math.sqrt(d.size) / 10 || 4.5)
  .style("fill", color)
  .on("click", click)
  .call(force.drag)
  
tick = () ->
  link.attr("x1", (d) -> d.source.x)
      .attr("y1", (d) -> d.source.y)
      .attr("x2", (d) -> d.target.x)
      .attr("y2", (d) -> d.target.y)

  node.attr("cx", (d) -> d.x)
      .attr("cy", (d) -> d.y)

# Color leaf nodes orange, and packages white or blue.
color = (d) -> 
  if d._children
    nodeColour = "#3182bd"
  else if d.children
    nodeColour = "#c6dbef"
  else
    nodeColour = "#fd8d3c"

# Toggle children on click.
click = (d) ->
  if !d3.event.defaultPrevented
    if d.children
      d._children = d.children
      d.children = null
    else 
      d.children = d._children
      d._children = null
    update()

# Returns a list of all nodes under the root.
nodes = []
i = 0
flatten = (root) -> 
  nodes = []
  i = 0
  recurse(root)
  return nodes
  
recurse = (node) ->
  if node.children 
    node.children.forEach(recurse)
  if !node.id 
    node.id = ++i
  nodes.push(node)



  

