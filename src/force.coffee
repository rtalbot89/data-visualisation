w = 1300
h = 800

query =
'prefix dct: <http://purl.org/dc/terms/>
prefix foaf: <http://xmlns.com/foaf/0.1/>
prefix skos: <http://www.w3.org/2004/02/skos/core#>
SELECT  ?sublabel ?fname WHERE {
?s dct:creator <http://wrap.warwick.ac.uk/id/person/ext-36435>.
?s dct:creator ?other.
?s dct:subject ?subject.
?subject skos:prefLabel ?sublabel.
?other foaf:name ?fname.
?s dct:title ?ptitle}'

d3.xhr("http://localhost:8080/openrdf-sesame/repositories/WRAP-1
?query=#{escape(query)}",
'application/sparql-results+json',
(error,data) ->
  jsonData = JSON.parse(data.response)
  # dataset initialisation
  dataset = {}
  dataset.nodes = new Array()
  dataset.edges = new Array()
	# this still looks over-complicated. There may be a better approach 
  # but I haven't found it yet
  nodeCheck = new Array()
      
  for instance in jsonData.results.bindings
    if instance.sublabel.value not in nodeCheck
      dataset.nodes.push({name: instance.sublabel.value})
      nodeCheck.push(instance.sublabel.value)
            
    if instance.fname.value not in nodeCheck
      dataset.nodes.push({name : instance.fname.value})
      nodeCheck.push(instance.fname.value)
    
    elementPos = dataset.nodes.map((x) -> x.name)
      .indexOf(instance.fname.value)
    subjectPos = dataset.nodes.map((x) -> x.name)
      .indexOf(instance.sublabel.value)
    dataset.edges.push({source: elementPos, target: subjectPos })

  #Initialize a default force layout, using the nodes and edges in dataset
  console.log(dataset)
	
  force = d3.layout.force()
    .nodes(dataset.nodes)
    .links(dataset.edges)
    .size([w, h])
    .linkDistance([300])
    .charge([-200])
    .start()
    
  colors = d3.scale.category10()
    
  svg = d3.select("body")
    .append("svg")
    .attr("width", w)
    .attr("height", h)
        
  edges = svg.selectAll("line")
    .data(dataset.edges)
    .enter()
    .append("line")
    .style("stroke", "#ccc")
    .style("stroke-width", 1)
  
  gnodes = svg.selectAll('g.node')
    .data(dataset.nodes)
    .enter()
    .append('g')
    .classed('gnode', true)
  
  #Create nodes as circles
  node = gnodes.append("circle")
    .attr("r", 10)
    .style("fill", (d, i) -> colors(i))
    .call(force.drag)
        
  #Append the labels to each group
  labels = gnodes.append("text").text((d) -> d.name)

  #console.log(labels)
  #Every time the simulation "ticks", this will be called
  force.on("tick", () ->
    edges
      .attr("x1", (d) -> d.source.x)
      .attr("y1", (d) -> d.source.y)
      .attr("x2", (d) -> d.target.x)
      .attr("y2", (d) -> d.target.y)
    gnodes
      .attr("transform", (d) -> 'translate(' + [d.x, d.y] + ')')
    )
)