# There's a high degree of encapsulation going on below
# the graph generation is completed encapsulated, and so is the
# code to create checkboxes etc. I think this is a 'good thing'!
# As everything is a variable the order of declaring variables/functions
# is critical... can't use a variable/function before it's declared
# Not a big problem but makes the flow a bit odd in places.

# Initialise the graph
# graphConfig object contains any relevant setup
render = (json, graphConfig) ->
  
  # sensible defaults if someone forgets
  w = graphConfig.w or 600
  h = graphConfig.h or 600
  el = graphConfig.el or "body"
  
  # layout is the object returned by the function
  # only used to access the update function
  layout = {}
  color = d3.scale.category20()
  svg = d3.select(el).append("svg")
  svg.attr("height", h).attr "width", w
  link = svg.selectAll(".link")
  node = svg.selectAll("g.gnode")
  tick = ->
    link.attr("x1", (d) ->
      d.source.x
    ).attr("y1", (d) ->
      d.source.y
    ).attr("x2", (d) ->
      d.target.x
    ).attr "y2", (d) ->
      d.target.y

    node.attr "transform", (d) ->
      "translate(" + d.x + "," + d.y + ")"


  force = d3.layout.force().charge(-150).linkDistance(150).size([
    w
    h
  ]).on("tick", tick)
  
  # this is the function that generates the visualisation
  # according to the filters selected further down
  start = ->
    
    # not too sure about blitzing
    # the visual elements here but it
    # seems the only workable way to clear
    # things. There may be options in using
    # exit.remove() and capturing the output...dunno
    svg.selectAll("circle").remove()
    svg.selectAll(".node-text").remove()
    link = link.data(force.links())
    link.enter().insert("line").attr "class", "link"
    link.exit().remove()
    node = node.data(force.nodes())
    node.enter().append("g").attr("class", "gnode").attr "index", (d) ->
      d.id

    node.append("circle").attr("class", (d) ->
      d.type + "-node"
    ).attr "r", (d) -> 5 * Math.sqrt(d.count)
    node.append("text").text((d) ->
      d.id
    ).attr("class", "node-text").attr("x", "15").attr "y", 5
    node.call(force.drag)
    node.exit().remove()
    force.start()

  
  # intialise the graph, called once only
  force.links json.links
  force.nodes json.nodes
  start()
  
  # filter nodes and links using the array passed in,
  # and update the graph
  layout.update = (filters) ->
    filteredNodes = json.nodes.filter((e) ->
      filters.indexOf(e.id) is -1
    )
    filteredLinks = json.links.filter((e) ->
      filters.indexOf(e.source.id) is -1 and filters.indexOf(e.target.id) is -1
    )
    force.links filteredLinks
    force.nodes filteredNodes
    start()

  # finally return the object used for
  # external function calls
  layout

# end of graph rendering function

#the rest is specific
#initialise with json 

dataset = {}
dataset.nodes = new Array()
dataset.links = new Array()

# different types of query
###
query =
'PREFIX foaf:<http://xmlns.com/foaf/0.1/>
PREFIX dct:<http://purl.org/dc/terms/>
PREFIX skos:<http://www.w3.org/2004/02/skos/core#>
PREFIX terms:<http://purl.org/dc/terms/>
SELECT ?publisher ?subject WHERE
{?s terms:publisher ?pub.
?pub foaf:name ?publisher.
?s dct:subject ?sub.
?sub skos:prefLabel ?subject} LIMIT 50'
###

query =
'PREFIX foaf:<http://xmlns.com/foaf/0.1/>
PREFIX dct:<http://purl.org/dc/terms/>
PREFIX skos:<http://www.w3.org/2004/02/skos/core#>
PREFIX terms:<http://purl.org/dc/terms/>
SELECT ?publisher ?subject WHERE
{?s terms:publisher ?pub.
?pub foaf:name ?publisher.
?s dct:subject ?sub.
?sub skos:prefLabel ?subject}'

(->
  d3.xhr("http://localhost:8080/openrdf-sesame/repositories/WRAP-1?query=#{escape(query)}",
  'application/sparql-results+json',(error,data) ->
    jsonData = JSON.parse(data.response)
    # this is just a temporary helper array
    # to track indices
    # may be worth trying something like map directly on the dataset to
    # compare performance
    nodeCheck = new Array()
    
    for instance in jsonData.results.bindings
      # see comment above
      subjectIndex = nodeCheck.indexOf(instance.subject.value)
      publisherIndex = nodeCheck.indexOf(instance.publisher.value)
      
      if subjectIndex is -1
        dataset.nodes.push({id: instance.subject.value, type : 'subject', count: 1})
        nodeCheck.push(instance.subject.value)
      else
        dataset.nodes[subjectIndex].count++
        
      if publisherIndex is -1
        dataset.nodes.push({id : instance.publisher.value, type : 'publisher', count: 1})
        nodeCheck.push(instance.publisher.value)
      else
        dataset.nodes[publisherIndex].count++
          
      elementPos = dataset.nodes.map((x) -> x.id)
      .indexOf(instance.publisher.value)
      
      subjectPos = dataset.nodes.map((x) -> x.id)
      .indexOf(instance.subject.value)
      dataset.links.push({source: nodeCheck.indexOf(instance.publisher.value), target: nodeCheck.indexOf(instance.subject.value) })
      
    graphConfig = {}
    graphConfig.w = "900"
    graphConfig.h = "600"
    graphConfig.el = "#graph"
    forceGraph = render(dataset, graphConfig)
    control = d3.select("#controls")
    button = control.append("button").text("choose")
    clear = control.append("button").text("clear")
    subjectList = control.append("div")
    publisherList = control.append("div")
    subjectList.append("h3").text("Subjects")
    publisherList.append("h3").text("Publishers")
    
    clear.on "click", ->
       checks = control.selectAll("input[type=checkbox]")
       checks.each () -> d3.select(this).property("checked", false)
       
      
    button.on "click", ->
      filters = []
      checks = control.selectAll("input:not(:checked)")
      checks.each -> filters.push d3.select(this).attr("value")
      forceGraph.update filters
      
    i = 0
    while i < dataset.nodes.length
      if dataset.nodes[i].type is "subject"
        span = subjectList.append("p")
      else
        span = publisherList.append("p")
        
      checkbox = span.append("input").attr("type", "checkbox").attr("name", "nodename").attr("checked", "checked").attr("value", dataset.nodes[i].id)
      span.append("label").text dataset.nodes[i].id
      span.append("span").text(" count: #{dataset.nodes[i].count}")
      i++
  )
).call this
