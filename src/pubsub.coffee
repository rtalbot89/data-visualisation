w = 1300
h = 800
  # dataset initialisation
dataset = {}
dataset.nodes = new Array()
dataset.edges = new Array()
node = null


query =
'PREFIX foaf:<http://xmlns.com/foaf/0.1/>
PREFIX dct:<http://purl.org/dc/terms/>
PREFIX skos:<http://www.w3.org/2004/02/skos/core#>
PREFIX terms:<http://purl.org/dc/terms/>
SELECT ?publisher ?subject WHERE
{?s terms:publisher ?pub.
?pub foaf:name ?publisher.
?s dct:subject ?sub.
?sub skos:prefLabel ?subject} LIMIT 50'

network = d3.xhr("http://localhost:8080/openrdf-sesame/repositories/WRAP-1
?query=#{escape(query)}",
'application/sparql-results+json',
(error,data) ->
  #.log data.response
  jsonData = JSON.parse(data.response)
	# this still looks over-complicated. There may be a better approach 
  # but I haven't found it yet
  nodeCheck = new Array()
  subjects = new Array()
  publishers = new Array()
      
  for instance in jsonData.results.bindings
    if instance.subject.value not in nodeCheck
      dataset.nodes.push({name: instance.subject.value, type : 'subject'})
      nodeCheck.push(instance.subject.value)
      subjects.push(instance.subject.value)
            
    if instance.publisher.value not in nodeCheck
      dataset.nodes.push({name : instance.publisher.value, type : 'publisher'})
      nodeCheck.push(instance.publisher.value)
      publishers.push(instance.publisher.value)
    
    elementPos = dataset.nodes.map((x) -> x.name)
      .indexOf(instance.publisher.value)
    subjectPos = dataset.nodes.map((x) -> x.name)
      .indexOf(instance.subject.value)
    dataset.edges.push({source: elementPos, target: subjectPos })

  #Initialize a default force layout, using the nodes and edges in dataset
  #console.log(subjects)
  addSubjects(subjects)
  #addPublishers(publishers)
	
  network.force = d3.layout.force()
    .nodes(dataset.nodes)
    .links(dataset.edges)
    .size([w, h])
    .linkDistance([300])
    .charge([-200])
    .start()
    
  colors = d3.scale.category10()
    
  network.svg = d3.select('#graph')
    .append("svg")
    .attr("width", w)
    .attr("height", h)
        
  network.edges = network.svg.selectAll("line")
    .data(dataset.edges)
    .enter()
    .append("line")
    .style("stroke", "#ccc")
    .style("stroke-width", 1)
  
  network.gnodes = network.svg.selectAll('g.node')
    .data(dataset.nodes)
    .enter()
    .append('g')
    .classed('gnode', true)
  
  #Create nodes as circles
  node = network.gnodes.append("circle")
    .attr("r", 10)
    .style("fill", (d) -> 
      if d.type == 'publisher'
        'teal'
      else 'gray'
    )
    .call(network.force.drag)
        
  #Append the labels to each group
  network.labels = network.gnodes.append("text").text((d) -> d.name)

  #console.log(labels)
  #Every time the simulation "ticks", this will be called
  network.force.on("tick", () ->
    network.edges
      .attr("x1", (d) -> d.source.x)
      .attr("y1", (d) -> d.source.y)
      .attr("x2", (d) -> d.target.x)
      .attr("y2", (d) -> d.target.y)
    network.gnodes
      .attr("transform", (d) -> 'translate(' + [d.x, d.y] + ')')
    )
)

addSubjects = (subjects) ->
  jQuery.each(subjects, (index, value) -> 
    checkbox = jQuery("<input type='checkbox' checked='checked' value='#{value}' name='subject-check' />")
    jQuery(checkbox).change(-> 
      #console.log dataset.edges
      #console.log dataset.nodes.length
      subjectId = dataset.nodes.map((x) -> x.name).indexOf(value)
      #console.log "subjectid #{subjectId}"
      removedNode = dataset.nodes.splice(subjectId , 1) 
      network.gnodes.splice(subjectId , 1)
      #console.log removedNode
      filteredEdges = (name for name in dataset.edges when name.target.index isnt subjectId)
      dataset.edges = filteredEdges
      #network.force.nodes(dataset.nodes)
      #network.force.links(dataset.edges)
      #network.edges.data(dataset.edges)
      #network.edges.shift()
      #network.edges.pop()
      #network.gnodes.data(dataset.nodes)
      #Append the labels to each group
      #labels = gnodes.append("text").text((d) -> d.name)
      #network.labels.remove()
      #network.labels = network.gnodes.append("text").text((d) -> d.name)
      network.force.start()
      
    )
    
    p = jQuery('</p>')
    jQuery(p).append(checkbox)
    jQuery(p).append("<span>#{value}</span>")
    jQuery('#subject-list').append(p)
  )
addPublishers = (publishers) ->
  jQuery.each(publishers, (index, value) ->
    jQuery('#publisher-list').append("<p>#{value}</p>")
  )
jQuery(document).ready ->
  #jQuery(':checkbox').change(-> alert('clicked'))
  console.log 'ready'
