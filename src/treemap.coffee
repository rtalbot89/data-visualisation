position = ->
  @style("left", (d) ->
    d.x + "px"
  ).style("top", (d) ->
    d.y + "px"
  ).style("width", (d) ->
    Math.max(0, d.dx - 1) + "px"
  ).style "height", (d) ->
    Math.max(0, d.dy - 1) + "px"

query =
 "prefix dct: <http://purl.org/dc/terms/>
  prefix foaf: <http://xmlns.com/foaf/0.1/>
  prefix skos: <http://www.w3.org/2004/02/skos/core#>
  SELECT ?subjectlabel (COUNT(DISTINCT ?paper) AS ?papers)
  (COUNT(DISTINCT ?author) AS ?authors)
  WHERE {
  ?paper dct:subject ?subject;
  dct:creator ?author.
  ?subject skos:prefLabel ?subjectlabel}
  GROUP BY ?subjectlabel"

margin =
  top: 40
  right: 10
  bottom: 10
  left: 10

width = 1100 - margin.left - margin.right
height = 700 - margin.top - margin.bottom
color = d3.scale.category20c()
treemap = d3.layout.treemap().size([
  width
  height
]).sticky(true).value((d) ->
  d.size
)
div = d3.select("body")
  .append("div")
  .style("position", "relative")
  .style("width", (width + margin.left + margin.right) + "px")
  .style("height", (height + margin.top + margin.bottom) + "px")
  .style("left", margin.left + "px").style("top", margin.top + "px")
d3.xhr "http://localhost:8080/openrdf-sesame/repositories/WRAP-1?query=#{escape(query)}",
'application/sparql-results+json',
(error, data) ->
  root =
    name: "flare"
    children: new Array()

  jsonData = JSON.parse(data.response)
  dataset = jsonData.results.bindings.map((obj) ->
    root.children.push
      name: obj.subjectlabel.value
      size: obj.papers.value
      authors: obj.authors.value
  )
  
  console.log root
  node = div.datum(root).selectAll(".node")
    .data(treemap.nodes)
    .enter()
    .append("div")
    .attr("class", "node")
    .call(position)
    .style("background", (d,i) ->color(i))
    .text((d) -> (if d.children then null else d.name))
  
  d3.selectAll("input").on "change", change = ->
    value = (if @value is "count" then (d) ->
      d.authors
     else (d) ->
      d.size
    )
    node.data(treemap.value(value).nodes)
    .transition().duration(1500).call position
