// Generated by CoffeeScript 1.6.3
(function() {
  var h, query, w,
    __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  w = 1300;

  h = 800;

  query = 'prefix dct: <http://purl.org/dc/terms/>\
prefix foaf: <http://xmlns.com/foaf/0.1/>\
prefix skos: <http://www.w3.org/2004/02/skos/core#>\
SELECT  ?sublabel ?fname WHERE {\
?s dct:creator <http://wrap.warwick.ac.uk/id/person/ext-36435>.\
?s dct:creator ?other.\
?s dct:subject ?subject.\
?subject skos:prefLabel ?sublabel.\
?other foaf:name ?fname.\
?s dct:title ?ptitle}';

  d3.xhr("http://localhost:8080/openrdf-sesame/repositories/WRAP-1?query=" + (escape(query)), 'application/sparql-results+json', function(error, data) {
    var colors, dataset, edges, elementPos, force, gnodes, instance, jsonData, labels, node, nodeCheck, subjectPos, svg, _i, _len, _ref, _ref1, _ref2;
    jsonData = JSON.parse(data.response);
    dataset = {};
    dataset.nodes = new Array();
    dataset.edges = new Array();
    nodeCheck = new Array();
    _ref = jsonData.results.bindings;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      instance = _ref[_i];
      if (_ref1 = instance.sublabel.value, __indexOf.call(nodeCheck, _ref1) < 0) {
        dataset.nodes.push({
          name: instance.sublabel.value
        });
        nodeCheck.push(instance.sublabel.value);
      }
      if (_ref2 = instance.fname.value, __indexOf.call(nodeCheck, _ref2) < 0) {
        dataset.nodes.push({
          name: instance.fname.value
        });
        nodeCheck.push(instance.fname.value);
      }
      elementPos = dataset.nodes.map(function(x) {
        return x.name;
      }).indexOf(instance.fname.value);
      subjectPos = dataset.nodes.map(function(x) {
        return x.name;
      }).indexOf(instance.sublabel.value);
      dataset.edges.push({
        source: elementPos,
        target: subjectPos
      });
    }
    console.log(dataset);
    force = d3.layout.force().nodes(dataset.nodes).links(dataset.edges).size([w, h]).linkDistance([300]).charge([-200]).start();
    colors = d3.scale.category10();
    svg = d3.select("body").append("svg").attr("width", w).attr("height", h);
    edges = svg.selectAll("line").data(dataset.edges).enter().append("line").style("stroke", "#ccc").style("stroke-width", 1);
    gnodes = svg.selectAll('g.node').data(dataset.nodes).enter().append('g').classed('gnode', true);
    node = gnodes.append("circle").attr("r", 10).style("fill", function(d, i) {
      return colors(i);
    }).call(force.drag);
    labels = gnodes.append("text").text(function(d) {
      return d.name;
    });
    return force.on("tick", function() {
      edges.attr("x1", function(d) {
        return d.source.x;
      }).attr("y1", function(d) {
        return d.source.y;
      }).attr("x2", function(d) {
        return d.target.x;
      }).attr("y2", function(d) {
        return d.target.y;
      });
      return gnodes.attr("transform", function(d) {
        return 'translate(' + [d.x, d.y] + ')';
      });
    });
  });

}).call(this);
