// There's a high degree of encapsulation going on below
// the graph generation is completed encapsulated, and so is the
// code to create checkboxes etc. I think this is a 'good thing'!
// As everything is a variable the order of declaring variables/functions
// is critical... can't use a variable/function before it's declared
// Not a big problem but makes the flow a bit odd in places.

// Initialise the graph
// graphConfig object contains any relevant setup
render = function(json, graphConfig){
  // sensible defaults if someone forgets
  var w = graphConfig.w || 600;
  var h = graphConfig.h || 600;
  var el = graphConfig.el || "body";
  // layout is the object returned by the function
  // only used to access the update function
  var layout = {};
  var color = d3.scale.category20();
  var svg = d3.select(el).append("svg");
  svg.attr("height", h).attr("width", w);

  var link = svg.selectAll(".link");
  var node = svg.selectAll("g.gnode");
  
  var tick = function () {
    link.attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

     node.attr("transform", function(d) { return 'translate(' + d.x + ',' + d.y +')';});
  };
  
  var force = d3.layout.force()
      .charge(-120)
      .linkDistance(50)
      .size([w, h])
      .on("tick", tick);
  
  // this is the function that generates the visualisation
  // according to the filters selected further down
  var start = function(){
    // not too sure about blitzing
    // the visual elements here but it
    // seems the only workable way to clear
    // things. There may be options in using
    // exit.remove() and capturing the output...dunno
    svg.selectAll(".node").remove(); 
    svg.selectAll(".node-text").remove();
    link = link.data(force.links());
    link.enter().insert("line").attr("class", "link");
    link.exit().remove();
    
    node = node.data(force.nodes());
    node.enter().append("g").attr("class", "gnode").attr("index", function(d) { return d.id;});
  
    node.append("circle")
          .attr("class", "node")
          .style("fill", function(d,i) { return color(i); })
          .attr("r", 10);
  
    node.append("text")
          .text(function(d){return d.id;})
          .attr("class","node-text")
          .attr("x", "15").attr("y",5);
    
    node.exit().remove();
    force.start();
  };
  
  // intialise the graph, called once only
  force.links(json.links);
  force.nodes(json.nodes);
  start();
  
  // filter nodes and links using the array passed in,
  // and update the graph
  layout.update = function(filters){
    var filteredNodes = json.nodes.filter(function(e){ return filters.indexOf(e.id) == -1});
    var filteredLinks = json.links.filter(function(e){ return filters.indexOf(e.source.id) == -1 && filters.indexOf(e.target.id) == -1});
    force.links(filteredLinks);
    force.nodes(filteredNodes);
    start();
  };
  
  // finally return the object used for
  // external function calls
  return layout;
};
// end of graph rendering function

//the rest is specific
//initialise with json 
(function(){
  d3.json("forcebasic.json", function(json){
    var graphConfig = {};
    graphConfig.w = "600";
    graphConfig.h = "600";
    graphConfig.el = "#graph";
    
    forceGraph = render(json, graphConfig);
    var control = d3.select("#controls");
    var button = control.append("button").text("choose");
    // function to send filters
    
    button.on("click", function(){
      // empty array to hold the filters. This is
      // regenerated every time the button is clicked
      // may not be an issue but something to look at in terms
      // of optimisation maybe.
      var filters = [];
      // Shame I can't figure out a more precise selector here just to
      // get an array of values... see below 
      var checks = control.selectAll("input:not(:checked)"); 
      // the following extracts the node values from each
      // checkbox. Seems a bit nasty to have to call each on the
      // elements...
      checks.each(function(){
        filters.push(d3.select(this).attr("value"));
      });
      
      // finally update the visualisation
      forceGraph.update(filters);
    });
    
    // generate the node controls, in this case
    // pre-checked checkboxes
    for (i=0; i < json.nodes.length; i++){
      var span = control.append("p");
      var checkbox = span.append("input")
      .attr("type", "checkbox")
      .attr("name", "nodename")
      .attr("checked","checked")
      .attr("value", json.nodes[i].id);
      span.append("label")
      .text(json.nodes[i].id);
    }  
  });
}).call(this);


