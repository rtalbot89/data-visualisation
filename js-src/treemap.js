var query =
['prefix dct: <http://purl.org/dc/terms/>',
'prefix foaf: <http://xmlns.com/foaf/0.1/>',
'prefix skos: <http://www.w3.org/2004/02/skos/core#>',
'SELECT ?subjectlabel (COUNT(DISTINCT ?paper) AS ?papers) (COUNT(DISTINCT ?author) AS ?authors)',
'WHERE {',
'?paper dct:subject ?subject;',
'dct:creator ?author.',
'?subject skos:prefLabel ?subjectlabel}',
'GROUP BY ?subjectlabel'
].join("\n");

var margin = {top: 40, right: 10, bottom: 10, left: 10},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

var color = d3.scale.category20c();

var treemap = d3.layout.treemap()
    .size([width, height])
    .sticky(true)
    .value(function(d) { return d.size; });

var div = d3.select("body").append("div")
    .style("position", "relative")
    .style("width", (width + margin.left + margin.right) + "px")
    .style("height", (height + margin.top + margin.bottom) + "px")
    .style("left", margin.left + "px")
    .style("top", margin.top + "px");


d3.xhr(
       "http://localhost:8080/openrdf-sesame/repositories/WRAP-1?query=" + escape(query),
       'application/sparql-results+json',
       function (error, data) {
        var root={"name":"flare", "children" : new Array()};
        var jsonData = JSON.parse(data.response);
        var dataset = jsonData.results.bindings.map(function(obj){
            root.children.push({"name": obj.subjectlabel.value, "size" : obj.papers.value, "authors": obj.authors.value});
            });
        console.log(root);

  var node = div.datum(root).selectAll(".node")
      .data(treemap.nodes)
    .enter().append("div")
      .attr("class", "node")
      .call(position)
      .style("background", function(d) { return d.children ? color(d.name) : null; })
      .text(function(d) { return d.children ? null : d.name; });

  d3.selectAll("input").on("change", function change() {
    var value = this.value === "count"
        ? function(d) { return d.authors; }
         : function(d) { return d.size; };

    node
        .data(treemap.value(value).nodes)
      .transition()
        .duration(1500)
        .call(position);
  });
});

function position() {
  this.style("left", function(d) { return d.x + "px"; })
      .style("top", function(d) { return d.y + "px"; })
      .style("width", function(d) { return Math.max(0, d.dx - 1) + "px"; })
      .style("height", function(d) { return Math.max(0, d.dy - 1) + "px"; });
}
